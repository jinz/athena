#include "RpcRawDataMonitoring/RpcRawDataValAlg.h"
#include "RpcRawDataMonitoring/RpcLv1RawDataValAlg.h"
#include "RpcRawDataMonitoring/RpcLv1RawDataSectorLogic.h"
#include "RpcRawDataMonitoring/RpcLv1RawDataEfficiency.h"
#include "RpcRawDataMonitoring/RPCStandaloneTracksMon.h"
  
DECLARE_COMPONENT( RpcRawDataValAlg )
DECLARE_COMPONENT( RpcLv1RawDataValAlg )
DECLARE_COMPONENT( RpcLv1RawDataSectorLogic )
DECLARE_COMPONENT( RpcLv1RawDataEfficiency )
DECLARE_COMPONENT( RPCStandaloneTracksMon )

